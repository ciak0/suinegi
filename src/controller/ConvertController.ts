import joi from 'joi';
import { Request, Response } from 'express';
import ExchangeRateService from '../service/ExchangeRateService';

const convertQuerySchema = joi.object({
  amount: joi.number().required(),
  src_currency: joi.string().length(3).uppercase().required(),
  dest_currency: joi.string().length(3).uppercase().required(),
  reference_date: joi.string().regex(/^\d{4}-(0[1-9]|1[0-2])-([0-2][1-9]|[1-2]0|3[0-1])$/).required(),
});

export default class ConvertController {
  private exchangeRateService: ExchangeRateService;

  constructor(exchangeRateService: ExchangeRateService) {
    this.exchangeRateService = exchangeRateService;
  }

  public convert = (req: Request, res: Response) => {
    const { value, error } = joi.validate<any>(req.query, convertQuerySchema);
    if (error) {
      return res.status(400).json({ error });
    }

    const rate = this.exchangeRateService.getRate(value.src_currency, value.dest_currency, value.reference_date);
    if (!rate) {
      return res.sendStatus(404);
    }

    return res.json({
      amount: value.amount * rate,
      currency: value.dest_currency,
    });
  };
}
