import express from 'express';
import config from './config/default';
import ConvertController from './controller/ConvertController';
import ExchangeRateService from './service/ExchangeRateService';

export default async function bootstrap() {
  const app = express();

  const exchangeRateService = new ExchangeRateService(config.exchangeRateUrl);
  const controller = new ConvertController(exchangeRateService);

  await exchangeRateService.start();

  app.get('/convert', controller.convert);
  app.listen(config.port);
}

if (!module.parent) {
  bootstrap().catch(() => process.exit(-1));
}
