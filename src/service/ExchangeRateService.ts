import assert from 'assert';
import got from 'got';
import { parseStringPromise } from 'xml2js';

export default class ExchangeRateService {
  private exchangeRateUrl: string;

  private started = false;

  private data = new Map<string, Map<string, number>>();

  private minDate: string | undefined;

  private maxDate: string | undefined;

  constructor(exchangeRateUrl: string) {
    this.exchangeRateUrl = exchangeRateUrl;
  }

  /**
   * parse given xml, saving its data in our map
   * @param xml
   */
  private async parse(xml: string) {
    this.data.clear();
    this.minDate = undefined;
    this.maxDate = undefined;

    const root = await parseStringPromise(xml);
    const days = root['gesmes:Envelope'].Cube[0].Cube;
    days.forEach((node: any) => {
      const date = node.$.time;
      const currencies = node.Cube;

      const dailyMap = currencies.reduce((map: Map<string, number>, currencyNode: any) => {
        const quote = currencyNode.$.currency;
        const rate = Number.parseFloat(currencyNode.$.rate);

        map.set(quote, rate);
        return map;
      }, new Map<string, number>());

      this.data.set(date, dailyMap);
      this.maxDate = this.maxDate || date; // we can simply save first date
      this.minDate = date; // we can simply save last seen date as they're ordered DESC
    });
  }

  /**
   * find first occurence of quote rate, by iterating back in time one day at a time
   * @param quote
   * @param isoDate
   */
  private findRate(quote: string, isoDate: string): number | undefined {
    // we can simply use lexigographic comparison here because of yyyy-mm-dd format
    if (isoDate > this.maxDate! || isoDate < this.minDate!) {
      return undefined;
    }

    // search date map, if found just return quote
    const dailyMap = this.data.get(isoDate);
    if (dailyMap) {
      return dailyMap.get(quote.toUpperCase());
    }

    // iterate to previous date
    const prevDate = new Date(isoDate);
    prevDate.setDate(prevDate.getDate() - 1);

    return this.findRate(quote, prevDate.toISOString().split('T')[0]);
  }

  public async start() {
    assert(!this.started, 'Service already started');

    const { body } = await got(this.exchangeRateUrl);
    await this.parse(body);

    this.started = true;
  }

  public getRate(base: string, quote: string, isoDate: string): number | undefined {
    assert(this.started, 'Service has not been started yet');

    if (base.toUpperCase() === quote.toUpperCase()) {
      return 1;
    }

    if (base.toUpperCase() === 'EUR') {
      return this.findRate(quote, isoDate);
    }

    const baseEur = this.getRate('EUR', base, isoDate);
    const quoteEur = this.getRate('EUR', quote, isoDate);
    if (!baseEur || !quoteEur) {
      return undefined;
    }

    return quoteEur / baseEur;
  }
}
