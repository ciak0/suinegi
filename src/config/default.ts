export default {
  port: 3000 || process.env.PORT,
  exchangeRateUrl: process.env.EXCHANGE_RATE_URL || 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml',
};
