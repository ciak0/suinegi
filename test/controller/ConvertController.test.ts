import { Request, Response } from 'express';
import ConvertController from '../../src/controller/ConvertController';
import ExchangeRateService from '../../src/service/ExchangeRateService';

const mockService = {
  getRate: jest.fn(),
} as any as ExchangeRateService;

describe('ConvertController', () => {
  beforeEach(() => {
    (mockService.getRate as jest.Mock).mockReturnValue(1.25);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('convert', () => {
    it('should respond correctly, using provided ExchangeRateService', () => {
      const controller = new ConvertController(mockService);
      const req = {
        query: {
          src_currency: 'eur',
          dest_currency: 'USD',
          amount: '10',
          reference_date: '2020-04-26',
        },
      } as any as Request;

      const res = { json: jest.fn() } as any as Response;

      controller.convert(req, res);
      expect(mockService.getRate).toHaveBeenCalledWith('EUR', 'USD', '2020-04-26');
      expect(res.json).toHaveBeenCalledWith({
        amount: 10 * 1.25,
        currency: 'USD',
      });
    });

    it('should respond with 404, when exchange rate service returns undefined', () => {
      (mockService.getRate as jest.Mock).mockReturnValue(undefined);

      const controller = new ConvertController(mockService);
      const req = {
        query: {
          src_currency: 'EUR',
          dest_currency: 'USD',
          amount: '10',
          reference_date: '2020-04-26',
        },
      } as any as Request;

      const res = { sendStatus: jest.fn() } as any as Response;

      controller.convert(req, res);
      expect(res.sendStatus).toHaveBeenCalledWith(404);
    });

    it.each([
      ['empty', {}],
      ['invalid src_currency format', { src_currency: 'EURX' }],
      ['invalid dst_currency format', { dst_currency: 'EURX' }],
      ['invalid amount format', { amount: '34.d' }],
      ['invalid reference_date format', { reference_date: '2000-43-12' }],
    ])('should reply with HTTP status code 400 when called with a request with %s query string', async (_, query) => {
      const controller = new ConvertController(mockService);
      const req = { query } as any as Request;
      const res = {
        status: jest.fn(),
        json: jest.fn(),
      } as any as Response;

      (res.status as jest.Mock).mockReturnValue(res);

      controller.convert(req, res);

      expect(res.status).toHaveBeenCalledWith(400);
      expect(res.json).toHaveBeenCalledWith(expect.any(Object));
    });
  });
});
