import got from 'got';
import { AssertionError } from 'assert';
import ExchangeRateService from '../../src/service/ExchangeRateService';

jest.mock('got', () => jest.fn());

const url = 'http://whatever.com/fun.xml';
const xml = `
<?xml version="1.0" encoding="UTF-8"?>
<gesmes:Envelope xmlns:gesmes="http://www.gesmes.org/xml/2002-08-01"
  xmlns="http://www.ecb.int/vocabulary/2002-08-01/eurofxref">
  <gesmes:subject>Reference rates</gesmes:subject>
  <gesmes:Sender>
    <gesmes:name>European Central Bank</gesmes:name>
  </gesmes:Sender>
  <Cube>
    <Cube time="2020-04-27">
      <Cube currency="USD" rate="1.0852"/>
      <Cube currency="JPY" rate="116.22"/>
      <Cube currency="BGN" rate="1.9558"/>
      <Cube currency="CZK" rate="27.184"/>
      <Cube currency="DKK" rate="7.4591"/>
      <Cube currency="GBP" rate="0.87263"/>
      <Cube currency="HUF" rate="354.57"/>
      <Cube currency="PLN" rate="4.5287"/>
      <Cube currency="RON" rate="4.837"/>
      <Cube currency="SEK" rate="10.8748"/>
      <Cube currency="CHF" rate="1.0557"/>
      <Cube currency="ISK" rate="158.8"/>
      <Cube currency="NOK" rate="11.4513"/>
      <Cube currency="HRK" rate="7.5525"/>
      <Cube currency="RUB" rate="80.4926"/>
      <Cube currency="TRY" rate="7.5775"/>
      <Cube currency="AUD" rate="1.6795"/>
      <Cube currency="BRL" rate="6.0328"/>
      <Cube currency="CAD" rate="1.5255"/>
      <Cube currency="CNY" rate="7.686"/>
      <Cube currency="HKD" rate="8.4104"/>
      <Cube currency="IDR" rate="16695.8"/>
      <Cube currency="ILS" rate="3.8109"/>
      <Cube currency="INR" rate="82.6195"/>
      <Cube currency="KRW" rate="1328.82"/>
      <Cube currency="MXN" rate="27.0245"/>
      <Cube currency="MYR" rate="4.725"/>
      <Cube currency="NZD" rate="1.7893"/>
      <Cube currency="PHP" rate="54.987"/>
      <Cube currency="SGD" rate="1.5391"/>
      <Cube currency="THB" rate="35.236"/>
      <Cube currency="ZAR" rate="20.4109"/>
    </Cube>
    <Cube time="2020-04-24">
      <Cube currency="BGN" rate="1.9558"/>
    </Cube>
  </Cube>
</gesmes:Envelope>
`;

describe('ExchangeRateService', () => {
  beforeEach(() => {
    (got as any as jest.Mock).mockResolvedValue({ body: xml });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('start', () => {
    it('should retrieve given file, parse it and store it, when correctly formatted', async () => {
      const service = new ExchangeRateService(url);

      await service.start();
      expect(got).toHaveBeenCalledWith(url);
    });

    it('should reject, when called twice', async () => {
      const service = new ExchangeRateService(url);

      await service.start();
      return expect(service.start()).rejects.toThrow(AssertionError);
    });

    it('should reject whenever a network error happens', () => {
      (got as any as jest.Mock).mockRejectedValue(new Error('Network error'));
      const service = new ExchangeRateService(url);

      return expect(service.start()).rejects.toThrow(Error);
    });

    it('should reject whenever file cannot be parsed', () => {
      (got as any as jest.Mock).mockResolvedValue('not an xml');
      const service = new ExchangeRateService(url);

      return expect(service.start()).rejects.toThrow(Error);
    });
  });

  describe('getRate', () => {
    it.each([
      ['EUR', 'EuR', 1],
      ['EUR', 'USd', 1.0852],
      ['eur', 'jpy', 116.22],
      ['usd', 'JPY', 116.22 / 1.0852],
      ['JPY', 'USD', 1.0852 / 116.22],
    ])('should return rate, between %s/%s correctly, using EUR as transit', async (base, quote, expected) => {
      const service = new ExchangeRateService(url);

      await service.start();

      expect(service.getRate(base, quote, '2020-04-27')).toEqual(expected);
    });

    it('should return rate using latest available date, when data point is not found', async () => {
      const service = new ExchangeRateService(url);

      await service.start();

      expect(service.getRate('EUR', 'BGN', '2020-04-26')).toEqual(1.9558);
    });

    it('should return undefined, when rate is not found in stored data', async () => {
      const service = new ExchangeRateService(url);

      await service.start();
      expect(service.getRate('EURA', 'USD', '2020-04-27')).toBeUndefined();
    });
  });
});
