import express from 'express';
import config from '../src/config/default';
import Server from '../src/server';

jest.mock('express', () => jest.fn());

const app = {
  get: jest.fn(),
  listen: jest.fn(),
};

describe('Server', () => {
  beforeEach(() => {
    (express as any as jest.Mock).mockReturnValue(app);
  });

  it('should bootstrap express server on configured port and expose /convert endpoint', async () => {
    await Server();

    expect(app.get).toHaveBeenCalledWith('/convert', expect.any(Function));
    expect(app.listen).toHaveBeenCalledWith(config.port);
  });
});
