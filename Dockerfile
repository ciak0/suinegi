FROM node:12.13.0

WORKDIR /suinegi

COPY . ./
RUN npm install && npm run build

CMD [ "node", "build/server.js" ]
