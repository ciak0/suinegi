### DESCRIPTION

Simple exchange rate service, that uses EUR as implicit base currency. Returns converted amount using latest available rate, for requested reference date.

Respond with HTTP 404 whenever rate cannot be found, or 400 whenever query parameters don't get validated.

The server runs on port configured through env var `PORT`, defaults to `3000`

### HOW TO RUN

You have two options:

- with Docker: `docker build -t suinegi . && docker run -p 3000:3000 suinegi`
- with nodejs: `npm i && npm start`

you can make requests at `http://localhost:3000/convert`
