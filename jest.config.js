module.exports = {
  coverageDirectory: '<rootDir>/test/_coverage_',
  preset: 'ts-jest',
  // The glob patterns Jest uses to detect test files
  testMatch: [
    '**/?(*.)+(spec|test).ts',
  ],
  // An array of regexp pattern strings that are matched against all test paths, matched tests are skipped
  testPathIgnorePatterns: [
    '<rootDir>/node_modules/',
    '<rootDir>/build/',
  ],
  verbose: true,
};
